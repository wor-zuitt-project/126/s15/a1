// HOMEWORK #1

function homework1(number1, number2) {

		if(number1 > number2) {
			console.log(number1)
		} else if (number1 < number2){
			console.log(number2)
		} else {
			console.log(`Number are the same which is ${number1}`)
		}
	} 
	

//HOMEWORK #2


function homework2(language){
		
	switch(language){
		case "French":
			console.log("Bonjour le monde");
			break 
		case "Spanish":
			console.log("Hola Mundo");
		    break
		case "Japanese":
			console.log("こんにちは世界");
		    break
		default:
			console.log("Please input the valid language");
		}

	}


//HOMEWORK 3
  function homework3(number, noun){
  	if(number === 1){
  		console.log(`I have ${number} ${noun}`)
  	} else if(number >= 1){
  		console.log(`I have ${number} ${noun}s`)
  	}
  }



//HOMEWORK4


  function homework4(score){
  	if (0 < score && 100 > score)
	  	{
	  		if(score > 90){
	  		  console.log("Your grade is A")
	  		}
	  		else if(80 < score < 89){
	  		  console.log("Your grade is B")
	  		}
	  		else if(70 < score < 79){
	  		  console.log("Your grade is C")
	  		}
	  		else if(60 < score < 69){
	  		  console.log("Your grade is D")
	  		}
	  		else if(score < 60){
	  		  console.log("Your grade is F")
	  		}
	  	} else {
	  		console.log("The paramenter must be between 0 and 100")
	  	}
  }


//HOMEWORK5 
  function homework5(number1, number2, operation){
  	switch(operation){
		case "+":
			console.log(number1 + number2);
			break 
		case "-":
			console.log(number1 - number2);
		    break
		case "*":
			console.log(number1 * number2);
		    break
		case "/":
			console.log(number1 / number2);
		    break
		}
  }

